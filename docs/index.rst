.. _manual-main:

.. |br| raw:: html

   <br />

.. _Enrico Seiler: enrico.seiler@fu-berlin.de
.. _Kathrin Trappe: kathrin.trappe@fu-berlin.de
.. _Issue Tracker: https://gitlab.com/eseiler/DaisySuite/issues

=================================
Welcome to DaisySuite's documentation!
=================================

DaisySuite is a mapping-based workflow for analyzing horizontal gene transfer (HGT) events in bacterial data. The Next Generation Sequencing (NGS) input is processed in two major steps. |br|\ First, DaisyGPS identifies possible acceptor and donor candidates. Second, Daisy determines exact HGT-regions for the acceptor/donor pairs.

---------------
Getting started
---------------

You can either install DaisySuite with :ref:`Conda <install_conda>` or :ref:`git <install_conda>`

--------
Citation
--------

* `Trappe, K., Marschall, T., Renard, B.Y.  "Detecting horizontal gene transfer by mapping sequencing reads across species boundaries". Bioinformatics. 2016 <https://doi.org/10.1093/bioinformatics/btw423>`_
* Seiler, E., Trappe, K., Renard, B.Y., Marschall, T. "Where did you come from, where did you go: Enhancing Metagenomic Approaches for Pathogen Identification". Manuscript in preparation. 2017

----------
Tools used
----------

* Bioconda: `Bioconda website <https://bioconda.github.io/>`_
* Biopython: `Cock, P.A., Antao, T., Chang, J.T., Chapman, B.A., Cox, C.J., Dalke, A., Friedberg, I., Hamelryck, T., Kauff, F., Wilczynski, B. and de Hoon, M.J.L. (2009) Biopython: freely available Python tools for computational molecular biology and bioinformatics. Bioinformatics <http://dx.doi.org/10.1093/bioinformatics/btp163>`_
* bwa: `Li, H. and Durbin, R. (2009) Fast and accurate short read alignment with Burrows-Wheeler Transform. Bioinformatics. <https://doi.org/10.1093/bioinformatics/btp324>`_
* Conda: `Conda website <https://conda.io/docs/>`_
* Gustaf: `Trappe, K., Emde, A., Ehrlich, H., Reinert, K. (2014) Gustaf: Detecting and correctly classifying SVs in the NGS twilight zone. Bioinformatics. <https://doi.org/10.1093/bioinformatics/btu431>`_
* Laser: `Marschall, T. and Schönhuth., A. (2013) LASER: Sensitive Long-Indel-Aware Alignment of Sequencing Reads. arXiv.. <https://arxiv.org/abs/1303.3520>`_
* Mason2: `Holtgrewe, M. (2010) Mason – A Read Simulator for Second Generation Sequencing Data. Technical Report FU Berlin. <http://publications.imp.fu-berlin.de/962/2/mason201009.pdf>`_
* MicrobeGPS: `Lindner, M.S., Renard, B.Y. (2015) Metagenomic Profiling of Known and Unknown Microbes with MicrobeGPS. PLoS ONE <https://doi.org/10.1371/journal.pone.0117711>`_
* NumPy: `van der Walt, S., Colber, S. and Varoquaux, G. (2011) The NumPy Array: A Structure for Efficient Numerical Computation. Computing in Science & Engineering. <http://dx.doi.org/10.1109/MCSE.2011.37>`_
* Pandas: `McKinney, W. (2010) Data Structures for Statistical Computing in Python. Proceedings of the 9th Python in Science Conference. <http://conference.scipy.org/proceedings/scipy2010/mckinney.html>`_
* pysam: `pysam website <https://github.com/pysam-developers/pysam>`_
* sak: `Github <https://github.com/seqan/seqan/tree/master/apps/sak>`_
* samtools: `Li H., Handsaker B., Wysoker A., Fennell T., Ruan J., Homer N., Marth G., Abecasis G., Durbin R. and 1000 Genome Project Data Processing Subgroup (2009) The Sequence alignment/map (SAM) format and SAMtools. Bioinformatics. <https://doi.org/10.1093/bioinformatics/btp352>`_
* SciPy: `Jones, E., Peterson, P., et al. (2001) SciPy: Open Source Scientific Tools for Python <http://www.scipy.org/>`_
* Snakemake: `Köster, J. and Rahmann, S. (2012) Snakemake - scalable bioinformatics workflow engine. Bioinformatics. <http://dx.doi.org/10.1093/bioinformatics/bts480>`_
* Stellar: `Kehr, B., Weese, D., Reiner, K. (2011) STELLAR: fast and exact local alignments. BMC Bioinformatics. <https://doi.org/10.1186/1471-2105-12-S9-S15>`_
* Yara: `Siragusa, E. (2015). Approximate string matching for high-throughput sequencing. PhD Dissertation, Free University of Berlin. <http://www.diss.fu-berlin.de/diss/servlets/MCRFileNodeServlet/FUDISS_derivate_000000017479/Diss_EnricoSiragusa_NoCV.pdf>`_

-------
Contact
-------

`Issue Tracker`_ |br|\
`Enrico Seiler`_ |br|\
`Kathrin Trappe`_

.. toctree::
   :name: toc
   :hidden:
   :maxdepth: 2

   self
   tutorial/install_conda
   tutorial/install_git
   tutorial/database
   tutorial/config
   tutorial/usage
   tutorial/example

..
  Indices and tables
  ==================
  * :ref:`genindex`
  * :ref:`modindex`
  * :ref:`search`
