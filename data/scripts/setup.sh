#! /bin/bash

set -e

if [ -z "$1" ]
	then
		directory="."
	else
		directory=$1
fi

### Create all directories
echo \#\#\# Creating directories
mkdir -p $directory/ncbi
mkdir -p $directory/yara
mkdir -p $directory/bwa
mkdir -p ../microbeGPS/data

### Get names.dmp and nodes.dmp
echo \#\#\# Get taxonomy
wget ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/taxdump.tar.gz
tar xfz taxdump.tar.gz names.dmp nodes.dmp
mv names.dmp ../microbeGPS/data/names.dmp
mv nodes.dmp ../microbeGPS/data/nodes.dmp
rm taxdump.tar.gz

### Download NCBI database
echo \#\#\# Download NCBI database
wget ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/bacteria/assembly_summary.txt
wget --directory-prefix $directory/ncbi -q ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/367/745/GCF_000367745.1_ASM36774v1/GCF_000367745.1_ASM36774v1_genomic.fna.gz
cat assembly_summary.txt | \
awk '{FS="\t"} !/^#/ $12 ~ "Complete Genome" {print $20}' | \
sed -r 's|(ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/[0-9]*/[0-9]*/[0-9]*/)(GCF_.+)|\1\2/\2_genomic.fna.gz|' | \
xargs -n 1 -P 12 wget --directory-prefix $directory/ncbi -q
rm assembly_summary.txt

### Extract NCBI database
echo \#\#\# Extract NCBI database
gzip -d $directory/ncbi/*_genomic.fna.gz

### Split fasta files
echo \#\#\# Split fasta files
for f in $directory/ncbi/*.fna;
do
    src/split_fasta.py $f
done

### Rename fasta files
echo \#\#\# Rename fasta files
for f in $directory/ncbi/*.fna;
do
	if [[ $(head -n 1 $f) != *"Plasmid"* ]] && [[ $(head -n 1 $f) != *"plasmid"* ]]
	then
		accession=$(sed 's/>\([A-Za-z0-9\._]*\) .*/\1/' <(head -n 1 $f))
		new=$(dirname $f)/$accession.fasta
       		mv $f $new
	else
		rm -f $f
	fi
done

### Get accession list
echo \#\#\# Get accession list
for i in $directory/ncbi/*.fasta*;
do
	n=$(sed 's/\(.*\)\.fasta.*/\1/' <(basename $i))
	cat <(echo $n) >> acc.txt
done

### Merge fasta files
echo \#\#\# Merge fasta files
cat $directory/ncbi/*.fasta > $directory/ncbi/reference.fasta

### Gzip fasta files
echo \#\#\# Gzip fasta files
gzip $directory/ncbi/*.fasta

### Get catalog
echo \#\#\# Get catalog
wget -T 1800 -qO- ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/accession2taxid/nucl_gb.accession2taxid.gz | gzip -dc > $directory/nucl_gb.accession2taxid
src/createMinimalMGPSCatalog.py $directory/nucl_gb.accession2taxid ../microbeGPS/data/names.dmp ../microbeGPS/data/bact.catalog -ref acc.txt
rm $directory/nucl_gb.accession2taxid

### Remove accession list
echo \#\#\# Remove accession list
rm acc.txt

### Create bwa index
echo \#\#\# Create bwa index
bwa index $directory/ncbi/reference.fasta.gz -p $directory/bwa/bwa_index

echo \#\#\# Create yara index
yara_indexer $directory/ncbi/reference.fasta.gz -o $directory/yara/yara_index

### Remove merged fasta file
echo \#\#\# Remove merged fasta file
rm $directory/ncbi/reference.fasta.gz
