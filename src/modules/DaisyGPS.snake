############
# Packages #
############
import datetime
import os
import platform
import pwd
import socket
import sys
import pandas as pd

###########
# Modules #
###########

########
# Info #
########
PYTHON_VERSION = sys.version.split(' ')[0]
USER = pwd.getpwuid(os.getuid()).pw_name
SERVER = socket.getfqdn()
CALL = ' '.join(sys.argv)
if platform.system() == 'Linux':
    MACHINE = '{} {}'.format(platform.linux_distribution()[0], platform.linux_distribution()[1])
elif platform.system() == 'Windows':
    MACHINE = '{} {} {}'.format(platform.win32_ver()[0], platform.win32_ver()[1], platform.win32_ver()[2])
elif platform.system() == 'Java':
    MACHINE = 'Java'
else:
    MACHINE = '{} {}'.format(platform.mac_ver()[0], platform.mac_ver()[1][0])

##########
# Config #
##########
try:
    os.makedirs(config['outputdir'], exist_ok=True)
except:
    pass
samexpand = srcdir('../../data/dependencies/samexpand')
microbegps = srcdir('../../data/microbeGPS/microbegpsEX.py')

##############
### Script ###
##############

rule gzip_reads:
    input:
        read1 = os.path.join(config['outputdir'], 'reads', '{sample}.1.fq'),
        read2 = (lambda wildcards: os.path.join(config['outputdir'], 'reads', '{}.2.fq'.format(wildcards.sample)) if os.path.exists(os.path.join(config['outputdir'], 'reads', '{}.2.fq'.format(wildcards.sample))) else os.path.join(config['outputdir'], 'reads', '{}.1.fq'.format(wildcards.sample))) if not config['sim'] else os.path.join(config['outputdir'], 'reads', '{sample}.2.fq')
    output:
        read1gz = os.path.join(config['outputdir'], 'reads', '{sample}.1.fq.gz')
    run:
        if input.read1 == input.read2:
            shell('gzip -f {input.read1}')
        else:
            shell('gzip -f {input.read1} {input.read2}')

if config['bwa']:
    rule mapping:
        input:
            read1 = os.path.join(config['outputdir'], 'reads', '{sample}.1.fq.gz'),
            read2 = (lambda wildcards: os.path.join(config['outputdir'], 'reads', '{}.2.fq.gz'.format(wildcards.sample)) if os.path.exists(os.path.join(config['outputdir'], 'reads', '{}.2.fq.gz'.format(wildcards.sample))) else os.path.join(config['outputdir'], 'reads', '{}.1.fq.gz'.format(wildcards.sample))) if not config['sim'] else os.path.join(config['outputdir'], 'reads', '{sample}.2.fq.gz')
        output:
            temp(os.path.join(config['outputdir'], 'mapping', '{sample}.bam'))
        threads:
            config['threads']
        run:
            if input.read2 == input.read1:
                shell("bwa mem -M -k 40 -Y -t {threads} {config[bwa_index]} {input.read1} 2>> {LOG} | samtools view -bho {output} - 2>&1 >> {LOG}")
            else:
                shell("bwa mem -M -k 40 -Y -t {threads} {config[bwa_index]} {input.read1} {input.read2} 2>> {LOG} | samtools view -bho {output} - 2>&1 >> {LOG}")
else:
    rule mapping:
        input:
            read1 = os.path.join(config['outputdir'], 'reads', '{sample}.1.fq.gz'),
            read2 = (lambda wildcards: os.path.join(config['outputdir'], 'reads', '{}.2.fq.gz'.format(wildcards.sample)) if os.path.exists(os.path.join(config['outputdir'], 'reads', '{}.2.fq.gz'.format(wildcards.sample))) else os.path.join(config['outputdir'], 'reads', '{}.1.fq.gz'.format(wildcards.sample))) if not config['sim'] else os.path.join(config['outputdir'], 'reads', '{sample}.2.fq.gz')
        output:
            temp(os.path.join(config['outputdir'], 'mapping', '{sample}.bam'))
        threads:
            config['threads']
        run:
            if input.read1 == input.read2:
                shell("yara_mapper {config[yara_index]} {input.read1} -t {threads} | {samexpand} --no-unal - - | samtools view -Sb - -o {output} 2>&1 >> {LOG}")
                #shell("yara_mapper {config[yara_index]} {input.read1} -t {threads} -sa record -o {output} 2>&1 >> {LOG}")
            else:
                shell("yara_mapper {config[yara_index]} {input.read1} {input.read2} -t {threads} | {samexpand} --no-unal - - | samtools view -Sb - -o {output} 2>&1 >> {LOG}")
                #shell("yara_mapper {config[yara_index]} {input.read1} {input.read2} -t {threads} -sa record -o {output} 2>&1 >> {LOG}")

rule microbeGPS:
    input:
        os.path.join(config['outputdir'], 'mapping', '{sample}.bam')
    output:
        temp(os.path.join(config['outputdir'], 'mgps', '{sample}.mgps'))
    shell:
        "python3 {microbegps} {input} {output} --min_unique_reads 0 --max_homogeneity 1 2>&1 >> {LOG}"

rule process:
    input:
        mgps = os.path.join(config['outputdir'], 'mgps', '{sample}.mgps')
    output:
        complete = os.path.join(config['outputdir'], 'candidates', '{sample}_complete.tsv'),
        acceptor = temp(os.path.join(config['outputdir'], 'process', '{sample}_acceptor.tsv')),
        donor = temp(os.path.join(config['outputdir'], 'process', '{sample}_donor.tsv')),
        accdon = temp(os.path.join(config['outputdir'], 'process', '{sample}_accdon.tsv'))
    message:
        "Processing."
    run:
        df = pd.read_table(input.mgps)
        df_split = df['Name'].apply(lambda x: pd.Series(x.split('||')))
        del df['Name']
        df_split.rename(columns={0:'Name',1:'Accession.Version',2:'TaxID',3:'Parent TaxID',4:'Species TaxID'},inplace=True)
        df = pd.concat([df, df_split], axis=1)
        df = df[['Candidate', 'Name', 'Accession.Version', 'TaxID', 'Parent TaxID', 'Species TaxID', 'Abundance', 'Num. Reads', 'Unique Reads', 'Coverage', 'Validity', 'Homogeneity', 'Mapping Error']]
        df = df[df['TaxID'] != '[not found]']
        df['Property Score'] = (df['Num. Reads'] / df['Num. Reads'].sum()) * (df['Validity'] - df['Homogeneity'])
        df['Property'] = df['Validity'] - df['Homogeneity']

        pd.options.mode.chained_assignment = None
        try:
            df = df.loc[~df['TaxID'].isin([str(item) for sublist in list(next(d for d in config['taxon_blacklist'] if wildcards.sample in d).values()) for item in sublist])]
        except:
            pass
        try:
            df = df.loc[~df['Species TaxID'].isin([str(item) for sublist in list(next(d for d in config['species_blacklist'] if wildcards.sample in d).values()) for item in sublist])]
        except:
            pass
        try:
            df = df.loc[~df['Parent TaxID'].isin([str(item) for sublist in list(next(d for d in config['parent_blacklist'] if wildcards.sample in d).values()) for item in sublist])]
        except:
            pass
        df.to_csv(output.complete, index=False, sep='\t')

        # filter candidates with same TaxID as speciesTaxID if flag is set in config
        # CAUTION do not filter if result would be empty
        if config['filter_species'] and not df.loc[df['TaxID'] != df['Species TaxID']].empty:
            df = df.loc[df['TaxID'] != df['Species TaxID']]
            shell("echo 'Species filter activated for sample {wildcards.sample}' >> {LOG}")
        else:
            shell("echo 'Species filter deactivated for sample {wildcards.sample}' >> {LOG}")

        # acceptors
        df.sort_values(by='Property Score', ascending=False, inplace=True)
        df.to_csv(output.acceptor, index=False, sep='\t')

        # donors
        df.sort_values(by='Property Score', ascending=True, inplace=True)
        df.to_csv(output.donor, index=False, sep='\t')

        # acceptor-like donors
        df_accdons = df[df['Property Score'] > 0]
        df_accdons.to_csv(output.accdon, index=False, sep='\t')

rule candidates:
    input:
        acceptor = os.path.join(config['outputdir'], 'process', '{sample}_acceptor.tsv'),
        donor = os.path.join(config['outputdir'], 'process', '{sample}_donor.tsv'),
        accdon = os.path.join(config['outputdir'], 'process', '{sample}_accdon.tsv')
    output:
        candidates = os.path.join(config['outputdir'], 'candidates', '{sample}_candidates.tsv'),
        acceptor = os.path.join(config['outputdir'], 'candidates', '{sample}_acceptors.txt'),
        donor = os.path.join(config['outputdir'], 'candidates', '{sample}_donors.txt')
    run:
        df_acceptor = pd.read_table(input.acceptor)
        del df_acceptor['Candidate']
        df_acceptor.insert(0, 'Type', 'Acceptor')
        df_acceptor_duplicates = df_acceptor.copy()
        #df_acceptor.drop_duplicates(subset='Species TaxID', inplace=True)

        df_donor = pd.read_table(input.donor)
        del df_donor['Candidate']
        df_donor.insert(0, 'Type', 'Donor')
        df_donor_duplicates = df_donor.copy()
        df_donor.drop_duplicates(subset='Species TaxID', inplace=True)
        #df_donor = df_donor[:5]
        df_donor.sort_values(by='Property', ascending=True, inplace=True)

        df_accdon = pd.read_table(input.accdon)
        del df_accdon['Candidate']
        df_accdon.insert(0, 'Type', 'Acceptor-like Donor')
        df_accdon_duplicates = df_accdon.copy()
        df_accdon.drop_duplicates(subset='Species TaxID', inplace=True)
        df_accdon.sort_values(by='Property', ascending=False, inplace=True)
        pd.options.mode.chained_assignment = None
        
        num_acc = config['number_acceptors']
        num_don = config['number_donors']
        num_accdon = config['number_accdons']

        df_candidates = pd.concat([df_acceptor[:num_acc], df_donor[:num_don], df_accdon[:num_accdon]], axis=0)
        df_candidates.to_csv(output.candidates, index=False, sep='\t')

        unreported_acceptors = dict()
        unreported_donors = dict()
        unreported_accdons = dict()

        try:
            unreported = set()
            taxid_acceptor = list(df_acceptor['TaxID'])[0]
            species_acceptor = list(df_acceptor['Species TaxID'])[0]
            parent_acceptor = list(df_acceptor['Parent TaxID'])[0]
            score = list(df_acceptor['Property Score'])[1]
            df_acceptor_duplicates = df_acceptor_duplicates[df_acceptor_duplicates['Property Score'] > score]
            id_acceptor = df_acceptor_duplicates[(df_acceptor_duplicates['Species TaxID'] == species_acceptor) & (df_acceptor_duplicates['TaxID'] != taxid_acceptor)]
            for acc in id_acceptor.iterrows():
                unreported.add('{}\t{}'.format(acc[1]['Accession.Version'], acc[1]['Name']))
            unreported_acceptors['Acceptors within same species as first acceptor and higher score than second acceptor:'] = unreported
        except IndexError:
            pass

        try:
            unreported = set()
            taxid_donor = list(df_donor['TaxID'])[0]
            species_donor = list(df_donor['Species TaxID'])[0]
            parent_donor = list(df_donor['Parent TaxID'])[0]
            score = list(df_donor['Property Score'])[1]
            df_donor_duplicates = df_donor_duplicates[df_donor_duplicates['Property Score'] < score]
            id_donor = df_donor_duplicates[(df_donor_duplicates['Species TaxID'] == species_donor) & (df_donor_duplicates['TaxID'] != taxid_donor)]
            for acc in id_donor.iterrows():
                unreported.add('{}\t{}'.format(acc[1]['Accession.Version'], acc[1]['Name']))
            unreported_donors['Donors within same species as first donor and lower score than second donor:'] = unreported
        except IndexError:
            pass

        try:
            unreported = set()
            taxid_donor = list(df_donor['TaxID'])[1]
            species_donor = list(df_donor['Species TaxID'])[1]
            parent_donor = list(df_donor['Parent TaxID'])[1]
            score = list(df_donor['Property Score'])[2]
            df_donor_duplicates = df_donor_duplicates[df_donor_duplicates['Property Score'] < score]
            id_donor = df_donor_duplicates[(df_donor_duplicates['Species TaxID'] == species_donor) & (df_donor_duplicates['TaxID'] != taxid_donor)]
            for acc in id_donor.iterrows():
                unreported.add('{}\t{}'.format(acc[1]['Accession.Version'], acc[1]['Name']))
            unreported_donors['Donors within same species as second donor and lower score than third donor:'] = unreported
        except IndexError:
            pass

        with open(output.candidates, 'a') as f:
            for key, value in unreported_acceptors.items():
                f.write('======================================\n')
                f.write('{}\n'.format(key))
                for accession in sorted(value):
                    f.write('{}\n'.format(accession))
            for key, value in unreported_donors.items():
                f.write('======================================\n')
                f.write('{}\n'.format(key))
                for accession in sorted(value):
                    f.write('{}\n'.format(accession))
            for key, value in unreported_accdons.items():
                f.write('======================================\n')
                f.write('{}\n'.format(key))
                for accession in sorted(value):
                    f.write('{}\n'.format(accession))

        id_acceptor = df_acceptor['Accession.Version'][:num_acc]
        id_donor = df_donor['Accession.Version'][:num_don]
        id_accdon = df_accdon['Accession.Version'][:num_accdon]
        with open(output.acceptor, 'w') as f:
            for acceptor in id_acceptor:
                f.write('{}\n'.format(acceptor))
        with open(output.donor, 'w') as f:
            for donor in pd.concat([id_donor, id_accdon], axis=0):
                f.write('{}\n'.format(donor))

rule references:
    input:
        acceptor = os.path.join(config['outputdir'], 'candidates', '{sample}_acceptors.txt'),
        donor = os.path.join(config['outputdir'], 'candidates', '{sample}_donors.txt')
    output:
        acceptor = os.path.join(config['outputdir'], 'candidates', '{sample}_acceptors.fa'),
        donor = os.path.join(config['outputdir'], 'candidates', '{sample}_donors.fa')
    run:
        shell('touch {output.acceptor}')
        shell('touch {output.donor}')
        with open(input.acceptor, 'r') as f:
            for line in f:
                acceptor = os.path.join(config['ncbidir'], line.strip()+'.fasta.gz')
                shell('gzip -dc {acceptor} >> {output.acceptor}')
        with open(input.donor, 'r') as f:
            for line in f:
                donor = os.path.join(config['ncbidir'], line.strip()+'.fasta.gz')
                shell('gzip -dc {donor} >> {output.donor}')

onstart:
    EXEC_TIME = datetime.datetime.strftime(datetime.datetime.now(), '%d-%m-%y_%H-%M')
    LOG = os.path.realpath(os.path.join(config['outputdir'], 'DaisyGPS_log-{}.txt'.format(EXEC_TIME)))
    with open(LOG, 'a') as f:
        f.write('Version: {}\n'.format(__version__))
        f.write('Execution time: {}\n'.format(EXEC_TIME))
        f.write('Python Version: {}\n'.format(PYTHON_VERSION))
        f.write('User: {}\n'.format(USER))
        f.write('Server: {}\n'.format(SERVER))
        f.write('Machine: {}\n'.format(MACHINE))
        f.write('Call: {}\n'.format(CALL))
onsuccess:
    shell('cat {log} >> {LOG}')
    shell('rm -d {} > /dev/null 2>&1 || true'.format(os.path.join(config['outputdir'], 'mgps')))
    shell('rm -d {} > /dev/null 2>&1 || true'.format(os.path.join(config['outputdir'], 'process')))
    shell('rm -d {} > /dev/null 2>&1 || true'.format(os.path.join(config['outputdir'], 'mapping')))
onerror:
    shell('cat {log} >> {LOG}')
